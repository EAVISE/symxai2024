var map = L.map('map').setView([51.44794,5.48434], 17);

L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
    maxZoom: 19,
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
}).addTo(map);

var marker = L.marker([51.44794,5.48434]).addTo(map);
marker.bindPopup("Auditorium 6 <br> 5612 AZ Eindhoven. <br>");

function onMapClick(e) {
    marker.openPopup()
}

map.on('click', onMapClick);